# Results Reporter

## Getting cutflows

#### Directly running program
```
python3 reporter.py [arguments] [options]
```
See below the list of arguments and options

#### Running shell script
```
./reportResults.sh
```

#### Arguments

```signal```
* Signal ntuple where cuts are going to be applied.
* Default: None

```bkg```
* Background ntuple where cuts are going to be applied.
* Default: None

```cutsList```
* Mandatory list of cuts that are going to be applied to the provided ntuples.
* Default: []

NOTE: At least a signal or a background ntuple must be provided.

#### Options

```label```
* Name that is going to be used to identify results.
* Default: "Default"

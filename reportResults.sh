# 1L gluino report using published cuts

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 700 && meffInc30 < 1100\"]"\
  label="gluinoStrong1L-paper-SR2J-bTag-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 1100 && meffInc30 < 1500\"]"\
  label="gluinoStrong1L-paper-SR2J-bTag-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 1500 && meffInc30 < 1900\"]"\
  label="gluinoStrong1L-paper-SR2J-bTag-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 1900\"]"\
  label="gluinoStrong1L-paper-SR2J-bTag-bin4"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 700 && meffInc30 < 1100\"]"\
  label="gluinoStrong1L-paper-SR2J-bVeto-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 1100 && meffInc30 < 1500\"]"\
  label="gluinoStrong1L-paper-SR2J-bVeto-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 1500 && meffInc30 < 1900\"]"\
  label="gluinoStrong1L-paper-SR2J-bVeto-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 6\", \"lep1Pt < 5*nJet30\", \"lep1Pt < 35\", \"nJet30 > 1\", \"met > 430\", \"mt > 100\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 1900\"]"\
  label="gluinoStrong1L-paper-SR2J-bVeto-bin4"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 1000 && meffInc30 < 1500\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bTag-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 1500 && meffInc30 < 2000\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bTag-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30>0\", \"meffInc30 > 2000\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bTag-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 1000 && meffInc30 < 1500\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bVeto-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 1500 && meffInc30 < 2000\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bVeto-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 300\", \"mt > 450\", \"LepAplanarity > 0.01\", \"met/meffInc30 > 0.25\", \"nBJet30<1\", \"meffInc30 > 2000\"]"\
  label="gluinoStrong1L-paper-SR4JHi-bVeto-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30>0\", \"meffInc30 > 1300 && meffInc30 < 1650\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bTag-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30>0\", \"meffInc30 > 1650 && meffInc30 < 2000\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bTag-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30>0\", \"meffInc30 > 2000\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bTag-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30<1\", \"meffInc30 > 1300 && meffInc30 < 1650\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bVeto-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30<1\", \"meffInc30 > 1650 && meffInc30 < 2000\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bVeto-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 3 && nJet30 < 6\", \"met > 250\", \"mt > 150 && mt < 450\", \"LepAplanarity > 0.05\", \"nBJet30<1\", \"meffInc30 > 2000\"]"\
  label="gluinoStrong1L-paper-SR4JLo-bVeto-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30>0\", \"meffInc30 > 700 && meffInc30 < 1233\"]"\
  label="gluinoStrong1L-paper-SR6J-bTag-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30>0\", \"meffInc30 > 1233 && meffInc30 < 1767\"]"\
  label="gluinoStrong1L-paper-SR6J-bTag-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30>0\", \"meffInc30 > 1767 && meffInc30 < 2300\"]"\
  label="gluinoStrong1L-paper-SR6J-bTag-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30>0\", \"meffInc30 > 2300\"]"\
  label="gluinoStrong1L-paper-SR6J-bTag-bin4"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30<1\", \"meffInc30 > 700 && meffInc30 < 1233\"]"\
  label="gluinoStrong1L-paper-SR6J-bVeto-bin1"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30<1\", \"meffInc30 > 1233 && meffInc30 < 1767\"]"\
  label="gluinoStrong1L-paper-SR6J-bVeto-bin2"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30<1\", \"meffInc30 > 1767 && meffInc30 < 2300\"]"\
  label="gluinoStrong1L-paper-SR6J-bVeto-bin3"

python3 reporter.py\
  signal="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_signal_GG_onestepCC_OneLepSkim_T_01_825_03_write1_nocut.root"\
  bkg="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/shionEvents/allTrees_bkg_data_OneLepSkim_T_01_825_03_nom_write1_nocut.root"\
  cutsList="[\"1\",\"nLep_signal>0 && nLep_signal<2\", \"lep1Pt > 35\", \"nJet30 > 5\", \"met > 350\", \"mt > 175\", \"LepAplanarity > 0.06\", \"nBJet30<1\", \"meffInc30 > 2300\"]"\
  label="gluinoStrong1L-paper-SR6J-bVeto-bin4"


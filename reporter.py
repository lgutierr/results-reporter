import sys
import os
import ROOT

#################################################
#
# Arguments
#
#################################################
signal = None
bkg = None
cutsList = []
label = "Default"

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "signal":
      try:
        global signal
        signal = v
      except:
        pass
    elif a == "bkg":
      try:
        global bkg
        bkg = v
      except:
        pass
    elif a == "cutsList":
      try:
        global cutsList
        cutsList = eval(v.replace(" ",""))
      except:
        pass
    elif a == "label":
      try:
        global label
        label = v
      except:
        pass
  return

def readArgs():
  print("----- Results reporter arguments -----")
  print("signal: "+str(signal))
  print("bkg: "+str(bkg))
  print("cutsList: "+str(cutsList))
  print("label: "+label)
  return

def checkArgs():
  print("----- Results reporter arguments check -----")
  accepted = True
  
  # Check signal and background argument
  if (signal == None and bkg == None) or (signal == None and len(bkg)==0) or (bkg == None and len(signal)==0):
    print("Please provide a valid signal and/or background...")
    accepted = False
  
  # Check cutsList argument
  if len(cutsList) == 0:
    print("Please provide a valid list of cuts...")
    accepted = False
 
  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...") 
    print("Exiting program without reporting...")
  return accepted


#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start event reporter log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./reporter.log"):
    f = open("./reporter.log", "w")
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching event reporter log file -----")
  
  # Check existing entries
  f = open('./reporter.log', 'r')
  # Variables
  reportCode = -1
  signalMatch = False
  bkgMatch = False
  cutsListMatch = False
  labelMatch = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check reportCode
    if code == "reportCode":
      # First entry
      if reportCode == -1:
        reportCode = int(value)
      # Following entries
      else:
        # Check if previous reportCode matches options
        match1 = signalMatch and bkgMatch
        match2 = cutsListMatch and labelMatch
        match = match1 and match2
        if match:
          break
        # If previous reportCode does not match options, update repoortCode and reset bools
        else:
          reportCode = int(value)
          signalMatch = False
          bkgMatch = False
          cutsListMatch = False
          labelMatch = False
    # Check signal
    elif code == "signal":
      if value == 'None':
        if signal == eval(value): signalMatch = True
      else:
        if signal == value: signalMatch = True
    # Check bkg
    elif code == "bkg":
      if value == 'None':
        if bkg == eval(value): bkgMatch = True
      else:
        if bkg == value: bkgMatch = True
    # Check cutsList
    elif code == "cutsList":
      print(value)
      print(eval(value))
      print(cutsList)
      if cutsList == eval(value): cutsListMatch = True
    # Check label
    elif code == "label":
      if label == value: labelMatch = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = signalMatch and bkgMatch
  match2 = cutsListMatch and labelMatch
  match = match1 and match2
  # Check the matching reportCode, otherwise create a new one
  if reportCode == -1:
    reportCode = 0
    match = False
  else:
    # If it does not match, generate a new reportCode
    if not match: reportCode += 1

  # Logging
  if match:
    print("Found report with exact same conditions...")
    print("Using reportCode: "+str(reportCode))
  else:
    print("No report with the same conditions found...")
    print("New reportCode: "+str(reportCode))

  # Return output
  return reportCode, match

def updateLog():
  print("----- Update event reporter log file -----")
  f = open("./reporter.log", "a")
  f.write("reportCode: "+str(reportCode)+"\n")
  f.write("  signal: "+str(signal)+"\n")
  f.write("  bkg: "+str(bkg)+"\n")
  f.write("  cutsList: "+str(cutsList)+"\n")
  f.write("  label: "+label+"\n")
  f.close()
  print("Added entry for reportCode="+str(reportCode))


#################################################
#
# Reporter
#
#################################################

def reporter():
  print("----- Report events -----")
  
  # Open output file
  outF = open("./results/{}/reportCode{}Cutflows.txt".format(label,reportCode), "w")
  outF.write("TreeN\tSelection\tWeightedEvents\n")

  # Process signal
  if signal != None:
    print("Processing signal...")
    
    # Apply cuts
    cutter(signal, outF)

  # Process bkg
  if bkg != None:
    print("Processing bkg...")
    
    # Apply cuts
    cutter(bkg, outF)

  # Close output file
  outF.close()

  return

def cutter(fPath, output):

  # Get consecutive cuts
  selectionStrs = []
  for i in range(len(cutsList)):
    if selectionStrs == []:
      selectionStrs.append(cutsList[i])
    else:
      selectionStrs.append(selectionStrs[i-1]+" && "+cutsList[i])

  # Open file
  inF = ROOT.TFile.Open(fPath, "READ")

  # Get keys
  keys = inF.GetListOfKeys()
  
  # Get tree names
  treeNs = [k.GetName() for k in keys if "data" not in k.GetName() and "weight" not in k.GetName()]
  treeNs.sort()

  # Loop through various TTrees
  for tree in treeNs:
  
    # Get TTree
    print("Opening TTree: "+tree)
    t = inF.Get(tree)

    # Get number of weighted events
    if tree == "data":
      weightStr = "1"
    else:
      # Removed pileupWeight due to possible bug that causes pileupWeight to be very large.
      weightStr = "36.1*1000*genWeight*eventWeight*leptonWeight*bTagWeight*ttNNLOWeight"
      if "GG_onestepCC" in tree:
        # There is a bug with some signal samples cross-sections that are set to -1
        # That xsec also affects genWeight
        fixedWeight = "(DSID == 371639 ? -0.0228 : "
        fixedWeight += "DSID == 371638 ? -0.0217 : "
        fixedWeight += "DSID == 371636 ? -0.0153 : "
        fixedWeight += "DSID == 371634 ? -0.00770 : "
        fixedWeight += "DSID == 371637 ? -0.00616 : "
        fixedWeight += "DSID == 371632 ? -0.00396 : "
        fixedWeight += "DSID == 371635 ? -0.00318 : "
        fixedWeight += "DSID == 371630 ? -0.00207 : "
        fixedWeight += "DSID == 371633 ? -0.00167 : "
        fixedWeight += "DSID == 371649 ? -0.00135 : "
        fixedWeight += "DSID == 371628 ? -0.00109 : "
        fixedWeight += "DSID == 371631 ? -0.000885 : "
        fixedWeight += "DSID == 371648 ? -0.000718 : "
        fixedWeight += "DSID == 371626 ? -0.000583 : "
        fixedWeight += "DSID == 371629 ? -0.000473 : "
        fixedWeight += "DSID == 371647 ? -0.000385 : "
        fixedWeight += "DSID == 371624 ? -0.000313 : "
        fixedWeight += "DSID == 371627 ? -0.000255 : "
        fixedWeight += "DSID == 371622 ? -0.000208 : "
        fixedWeight += "DSID == 371646 ? -0.000208 : "
        fixedWeight += "DSID == 371619 ? -0.000169 : "
        fixedWeight += "DSID == 371623 ? -0.000169 : "
        fixedWeight += "DSID == 371620 ? -0.000138 : "
        fixedWeight += "DSID == 371625 ? -0.000138 : "
        fixedWeight += "DSID == 371621 ? -0.000113 : "
        fixedWeight += "DSID == 371645 ? -0.000113 : "
        fixedWeight += "DSID == 371643 ? -0.0000918 : "
        fixedWeight += "DSID == 371640 ? -0.0000749 : "
        fixedWeight += "DSID == 371644 ? -0.0000749 : "
        fixedWeight += "DSID == 371641 ? -0.0000611 : "
        fixedWeight += "DSID == 371642 ? -0.0000499 : "
        fixedWeight += "1)"
        # Update weight string
        weightStr += "*"+fixedWeight
    var = "EventNumber"
    cutflow = []
    for sel in selectionStrs:
      t.Draw(var, "{}*({})".format(weightStr, sel))
      htemp = ROOT.gPad.GetPrimitive("htemp")
      integral = 0
      try:
        integral = htemp.Integral()
      except:
        pass
      cutflow.append(integral)

    # Save results
    for i in range(len(selectionStrs)):
      output.write("{}\t{}\t{}\n".format(tree, selectionStrs[i], cutflow[i]))

  return
    

#################################################
#
# Report results
#
#################################################
reportCode = -1

def main(**kwargs):
  '''
  Mandatory arguments:
    1. signal:
        Signal ntuple to perform cuts on
        Default: None
    2. bkg:
        Background ntuple to perform cuts on
        Default: None
    3. cutsList:
        List of cuts to apply for signal and bkg if available
        Default: []
    4. label:
        Prefix to use for distinguishing report
        Default: "Default"
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return
  
  # Start log file
  startLog()

  # Search log file for similar reports
  global reportCode
  reportCode, match = searchLog()

  # Create output directory
  currentDirs = os.listdir("./")
  if "results" not in currentDirs:
    os.system("mkdir results")
  resultsDirs = os.listdir("./results")
  if label not in resultsDirs:
    os.system("mkdir ./results/"+label)
  
  # Report results
  reporter()
  
  # Update log file
  if not match: updateLog()

  # End of report results
  print("----- END OF EVENT REPORTER -----")
  print("Thanks for using the event reporter algorithm")
  print("Best,")
  print("Luis Felipe Gutierrez on behalf of the UPenn team")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
